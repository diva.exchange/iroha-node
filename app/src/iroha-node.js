/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import fs from 'fs-extra'
import get from 'simple-get'
import http from 'http'
import { Iroha } from './iroha'
import { Logger } from '@diva.exchange/diva-logger'
import path from 'path'
import tunnel from 'tunnel'

const API_VERSION = '0.1.0-alpha'
const TIMEOUT_REQUEST_TUNNEL_MS = 30000 // 30 secs

const URL_I2P_PING_DEFAULT = 'http://diva.i2p/ping'

export class IrohaNode {
  /**
   * Constructor
   *
   * @param config
   *
   * @typedef {Object} config
   * @property {string} API_BUILD
   * @property {string} ip_listen
   * @property {number} port_listen
   * @property {string} torii
   * @property {string} account_id
   * @property {string} i2p_hostname
   * @property {number} i2p_port_http_proxy
   * @property {number} i2p_port_webconsole
   * @property {?array} bootstrap_peer
   * @property {?string} path_iroha
   * @property {?string} log_name
   * @property {?string} log_level
   */
  constructor (config) {
    this._versionApi = API_VERSION + '-' + config.API_BUILD
    this._ip = config.ip_listen
    this._port = config.port_listen
    this._hostnameI2P = config.i2p_hostname
    this._portHttpProxyI2P = config.i2p_port_http_proxy
    this._portWebconsoleI2P = config.i2p_port_webconsole
    this._bootstrapPeer = config.bootstrap_peer

    this._iroha = null
    this._torii = config.torii
    this._idPeer = ''
    this._pubkeyPeer = ''
    this._idAccount = config.account_id
    this._pubkeyAccount = ''
    this._pathIroha = config.path_iroha

    this._confirm = new Map()
    this._apiEndpoint = ''

    this._syncRound = 0

    this._init()
  }

  /**
   * @public
   */
  async shutdown () {
    this._mapTimeout.forEach((v) => { clearTimeout(v) })

    if (typeof this._api !== 'undefined' && this._api) {
      await this._api.close()
      this._api = null
    }
  }

  /**
   * Initialize API
   *
   * @throws {Error}
   * @private
   */
  _init (url = URL_I2P_PING_DEFAULT) {
    // check path
    if (!fs.existsSync(path.join(this._pathIroha, 'blockstore')) ||
      !fs.existsSync(path.join(this._pathIroha, 'data'))) {
      throw new Error(`Path to iroha not accessible: ${this._pathIroha}`)
    }

    Logger.info('Testing network connectivity...')
    this._tunnelRequest(url).then(() => {
      Logger.info('Network available. Looking for API endpoints...')

      // scrape local API endpoints
      const options = {
        url: `http://${this._hostnameI2P}:${this._portWebconsoleI2P}/?page=i2p_tunnels`
      }
      get.concat(options, (error, response, data) => {
        if (error) {
          Logger.warn('Could not access I2P webconsole. Will retry in 30s.').trace(error)
          this._timeout('init', () => { this._init() }, 30000)
          return
        }
        const reS = /Server Tunnels:(.+)$/s
        const m = reS.exec(data.toString())
        if (m && m[1]) {
          const reB32 = /b32=[^>]*>([^<]+).+?([a-z0-9]+\.b32\.i2p):80[^\d]/g
          const match = reB32.exec(m[1])
          this._apiEndpoint = match && match[2] ? match[2] : ''
        }
        if (!this._apiEndpoint) {
          throw new Error('No API endpoint found.')
        }
        Logger.info(`Serving API endpoint: ${this._apiEndpoint}`)

        this._createServer()
        this._initIroha()
        this._cleanConfirm()

        this._register()
      })
    }).catch(() => {
      Logger.warn('Network not available. Will retry in 30s.')
      this._timeout('init', () => { this._init() }, 30000)
    })
  }

  /**
   * Create the API HTTP server
   *
   * @private
   */
  _createServer () {
    this._api = http.createServer((req, res) => {
      req.method === 'GET' ? this._processApiRequest(req, res) : IrohaNode._errorRequest(res, 403)
    })
      .listen(this._port, this._ip, () => {
        Logger.info(`API (${this._versionApi}) listening on ${this._ip}:${this._port}`)
      })
      .on('close', () => {
        Logger.info(`API (${this._versionApi}) closing ${this._ip}:${this._port}`)
      })
  }

  /**
   * @param t {number} Retry on failure, in milliseconds
   * @private
   */
  _initIroha (t = 5000) {
    (async () => {
      try {
        this._idPeer = fs.readFileSync(
          path.join(this._pathIroha, 'data/name.key')).toString().trim()
        this._pubkeyPeer = fs.readFileSync(
          path.join(this._pathIroha, `data/${this._idPeer}.pub`)).toString().trim()
        this._pubkeyAccount = fs.readFileSync(
          path.join(this._pathIroha, `data/${this._idAccount}.pub`)).toString().trim()
        this._iroha = await Iroha.make(this._torii, this._idAccount)
        Logger.info('Iroha ready')
          .info(`Path ${this._pathIroha}`)
          .info(`Torii ${this._torii}`)
          .info(`Peer ${this._idPeer}`)
          .info(`Public Key Peer ${this._pubkeyPeer}`)
          .info(`Account ${this._idAccount}`)
          .info(`Public Key Account ${this._pubkeyAccount}`)

        // synchronize
        this._checkSyncStateIroha()
      } catch (error) {
        Logger.warn('Iroha not ready yet...').trace(error)
        this._timeout('initIroha', () => { this._initIroha(t < 30000 ? Math.floor(t * 1.5) : t) }, t)
      }
    })()
  }

  /**
   * Clean up the confirmation map (garbage collector)
   *
   * @private
   */
  _cleanConfirm () {
    // clean this._confirm, garbage collector
    const ts = Math.floor(Date.now() / 1000) - 300 // 300sec/5min ago
    this._confirm.forEach((v, k) => {
      if (v.timestamp < ts) {
        this._confirm.delete(k)
      }
    })

    this._timeout('gc', () => { this._cleanConfirm() }, 120000)
  }

  /**
   * @private
   */
  _register () {
    if (!this._bootstrapPeer.length) {
      return
    }
    this._shuffleBootstrapPeer()

    const api = this._bootstrapPeer[0]
    const address = this._apiEndpoint
    this._tunnelRequest(`http://${api}/peer?address=${address}`)
      .catch((status) => {
        Logger.trace(status)
        if (status !== 404) {
          return
        }
        let key = this._pubkeyPeer
        let url = new URL(
          `http://${this._ip}:${this._port}/peer/apply?action=add&api=${api}&address=${address}&key=${key}`,
          `http://${this._ip}:${this._port}`
        )
        this._applyPeer(url.searchParams)
          .then((json) => {
            Logger.info(`Registered peer ${address}`).trace(json)
          })
          .catch((error) => {
            Logger.warn(`Registration of peer failed ${address}`).trace(error)
          })

        const account = this._idAccount
        key = this._pubkeyAccount
        url = new URL(
          `http://${this._ip}:${this._port}/account/apply?&api=${api}&account=${account}&key=${key}`,
          `http://${this._ip}:${this._port}`
        )
        this._applyAccount(url.searchParams)
          .then((json) => {
            Logger.info(`Registered account ${account}`).trace(json)
          })
          .catch((error) => {
            Logger.warn(`Registration of account failed ${account}`).trace(error)
          })
      })
  }

  /**
   * @param ident {string}
   * @param handler {function}
   * @param msTimeout {number}
   * @private
   */
  _timeout (ident, handler, msTimeout) {
    if (!this._mapTimeout) {
      this._mapTimeout = new Map()
    }
    clearTimeout(this._mapTimeout.get(ident))
    this._mapTimeout.set(ident, setTimeout(handler, msTimeout))
  }

  /**
   * @private
   */
  _shuffleBootstrapPeer () {
    // durstenfeld shuffle
    for (let i = this._bootstrapPeer.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [this._bootstrapPeer[i], this._bootstrapPeer[j]] = [this._bootstrapPeer[j], this._bootstrapPeer[i]]
    }
  }

  /**
   * @private
   */
  _checkSyncStateIroha () {
    this._shuffleBootstrapPeer()

    const url = 'http://' + this._bootstrapPeer[0] + '/block'
    this._tunnelRequest(url).then((data) => {
      try {
        const block = JSON.parse(data)
        const id = block.blockV1.payload.height.toString().padStart(16, '0')
        if (fs.existsSync(path.join(this._pathIroha, 'blockstore', id))) {
          this._syncRound = 0
          return
        }
        if (this._syncRound++ > 2) {
          this._syncRound = 0
          this._syncIroha()
        }
      } catch (error) {
        Logger.warn('_checkSyncStateIroha failed').trace(error)
      }
    }).catch(() => {
      Logger.warn('_checkSyncStateIroha failed')
    })

    this._timeout('syncState', () => { this._checkSyncStateIroha() }, 60000)
  }

  /**
   * @throws {Error}
   * @private
   */
  _syncIroha () {
    const url = 'http://' + this._bootstrapPeer[0] + '/blockstore'
    this._tunnelRequest(url).then((data) => {
      fs.writeFile(path.join(this._pathIroha, 'import/blockstore.tar.xz'), data, (error) => {
        if (error) {
          throw error
        }
        Logger.info('Blockstore ready to import: ' + path.join(this._pathIroha, 'import/blockstore.tar.xz'))
        fs.writeFile(path.join(this._pathIroha, 'import/sigterm'), '', (error) => {
          if (error) {
            throw error
          }
          Logger.info('Sent sigterm to iroha')
        })
      })
    }).catch(() => {
      Logger.warn('_syncIroha failed')
    })
  }

  /**
   * @param req Request
   * @param res Response
   * @private
   */
  _processApiRequest (req, res) {
    const host = req.headers.host
    const url = new URL(req.url, `http://${host}`)
    const _p = url.pathname.replace(/\/+$/, '')

    switch (_p) {
      case '':
        return IrohaNode._endRequest(res, {
          version: this._versionApi,
          endpoint: this._apiEndpoint,
          key: this._pubkeyPeer
        })
      case '/blockstore':
        return this._blockstore(res)
      case '/block':
        return this._block(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/peer':
        return this._getPeer(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/peer/apply':
        return this._applyPeer(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/peer/modify':
        return this._modifyPeer(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/peer/confirm':
        return this._confirmPeer(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/account/apply':
        return this._applyAccount(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/account/create':
        return this._createAccount(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
      case '/account/confirm':
        return this._confirmAccount(url.searchParams)
          .then((json) => { IrohaNode._endRequest(res, json) })
          .catch((status) => { IrohaNode._errorRequest(res, status) })
    }
    IrohaNode._errorRequest(res, 403)
  }

  /**
   * @param res
   * @param json
   * @private
   */
  static _endRequest (res, json) {
    res.setHeader('Content-Type', 'application/json; charset=utf-8')
    res.statusCode = 200
    res.end(JSON.stringify(json))
  }

  /**
   * @param res
   * @param status {Number}
   * @private
   */
  static _errorRequest (res, status) {
    res.statusCode = status
    res.end()
  }

  /**
   * @paran res
   * @private
   */
  _blockstore (res) {
    const pathBlockstore = path.join(this._pathIroha, 'export/blockstore.tar.xz')
    if (fs.existsSync(pathBlockstore)) {
      res.setHeader('Content-Disposition', 'attachment; filename=blockstore.tar.xz')
      res.setHeader('Content-Type', 'application/octet-stream')
      res.setHeader('Content-Length', fs.statSync(pathBlockstore).size)
      fs.createReadStream(pathBlockstore).pipe(res)
      return
    }
    IrohaNode._errorRequest(res, 404)
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _block (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        let id = searchParams.get('id')
        id = Number(id) ? id.toString().padStart(16, '0') : ''
        const pathLatest = path.join(this._pathIroha, 'export/latest')
        if (!id && fs.existsSync(pathLatest)) {
          id = fs.readFileSync(pathLatest).toString().trim()
        }
        if (id) {
          const pathBlock = path.join(this._pathIroha, 'blockstore', id)
          if (fs.existsSync(pathBlock)) {
            return resolve(JSON.parse(fs.readFileSync(pathBlock)))
          }
        }
        reject(404)
      } catch (error) {
        Logger.warn('_block failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _applyPeer (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const action = IrohaNode._validateAction(searchParams.get('action'))
        const api = Iroha.validateHostname(searchParams.get('api'))
        const address = Iroha.validateHostname(searchParams.get('address'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const url = `http://${api}/peer/modify?action=${action}&address=${address}&key=${key}`
        this._tunnelRequest(url)
          .then(() => {
            this._confirm.set(action + ':' + address,
              { key: key, timestamp: Math.floor(Date.now() / 1000) })
            Logger.info(`_applyPeer: applied to ${action} ${address} at ${api}`)
            resolve({ action: action, api: api, address: address, key: key })
          })
          .catch(() => {
            Logger.warn(`_applyPeer: failed to apply to ${action} ${address} at ${api}`)
            reject(503)
          })
      } catch (error) {
        Logger.warn('_applyPeer failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _modifyPeer (searchParams) {
    return new Promise((resolve, reject) => {
      if (!this._iroha) {
        return reject(503)
      }

      try {
        const action = IrohaNode._validateAction(searchParams.get('action'))
        const address = Iroha.validateHostname(searchParams.get('address'))
        const key = Iroha.validateKey(searchParams.get('key'))
        this._timeout('_modifyPeer', () => {
          this._sendConfirmPeer(action, address, key)
        }, 30000)
        Logger.info(`_modifyPeer: request to ${action} ${address}`)
        resolve({ action: action, address: address, key: key })
      } catch (error) {
        Logger.warn('_modifyPeer failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param action {string}
   * @param address {string}
   * @param key {string}
   * @private
   */
  _sendConfirmPeer (action, address, key) {
    const url = `http://${address}/peer/confirm?action=${action}&address=${address}&key=${key}`
    this._tunnelRequest(url)
      .then(() => {
        switch (action) {
          case 'add':
            this._iroha.addPeer(address, key)
              .then(() => {
                Logger.info(`_modifyPeer: added peer ${address}`)
              })
              .catch((error) => {
                Logger.warn(`_modifyPeer: failed to add ${address}`).trace(error)
              })
            break
          case 'remove':
            this._iroha.removePeer(key)
              .then(() => {
                Logger.info(`_modifyPeer: removed peer ${address}`)
              })
              .catch((error) => {
                Logger.warn(`_modifyPeer: failed to remove ${address}`).trace(error)
              })
            break
        }
      })
      .catch((error) => {
        Logger.warn(`_modifyPeer: requesting ${url} failed`).trace(error)
      })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _confirmPeer (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const action = IrohaNode._validateAction(searchParams.get('action'))
        const address = Iroha.validateHostname(searchParams.get('address'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const lookup = action + ':' + address
        const obj = this._confirm.get(lookup)
        if (!obj) {
          Logger.warn(`_confirmPeer: ${lookup} not found`)
          reject(404)
        } else if (obj.key === key) {
          Logger.info(`_confirmPeer: ${lookup} confirmed`)
          resolve({ action: action, address: address, key: key })
        } else {
          Logger.warn(`_confirmPeer: ${lookup} failed`)
          reject(403)
        }
      } catch (error) {
        Logger.warn('_confirmPeer failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _applyAccount (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const api = Iroha.validateHostname(searchParams.get('api'))
        const address = this._apiEndpoint
        const account = Iroha.validateAccount(searchParams.get('account'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const url = `http://${api}/account/create?address=${address}&account=${account}&key=${key}`
        this._tunnelRequest(url)
          .then(() => {
            this._confirm.set('account:' + account,
              { key: key, timestamp: Math.floor(Date.now() / 1000) })
            Logger.info(`_applyAccount: applied to create ${account} at ${api}`)
            resolve({ api: api, account: account, key: key })
          })
          .catch(() => {
            Logger.warn(`_applyAccount: failed to apply to create ${account} at ${api}`)
            reject(503)
          })
      } catch (error) {
        Logger.warn('_applyAccount failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _createAccount (searchParams) {
    return new Promise((resolve, reject) => {
      if (!this._iroha) {
        return reject(503)
      }

      try {
        const address = Iroha.validateHostname(searchParams.get('address'))
        const account = Iroha.validateAccount(searchParams.get('account'))
        const key = Iroha.validateKey(searchParams.get('key'))
        this._timeout('_createAccount', () => {
          this._sendConfirmAccount(address, account, key)
        }, 30000)
        Logger.info(`_createAccount: request to create ${account}`)
        resolve({ account: account, key: key })
      } catch (error) {
        Logger.warn('_createAccount failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param address
   * @param account
   * @param key
   * @private
   */
  _sendConfirmAccount (address, account, key) {
    const url = `http://${address}/account/confirm?account=${account}&key=${key}`
    this._tunnelRequest(url)
      .then(() => {
        const [nameAccount, idDomain] = account.split('@')
        this._iroha.createAccount(nameAccount, idDomain, key)
          .then(() => {
            Logger.info(`_createAccount: created ${account}`)
          })
          .catch((error) => {
            Logger.warn(`_createAccount: failed to create ${account}`).trace(error)
          })
      })
      .catch((error) => {
        Logger.warn(`_createAccount: requesting ${url} failed`).trace(error)
      })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _confirmAccount (searchParams) {
    return new Promise((resolve, reject) => {
      try {
        const account = Iroha.validateAccount(searchParams.get('account'))
        const key = Iroha.validateKey(searchParams.get('key'))

        const lookup = 'account:' + account
        const obj = this._confirm.get(lookup)
        if (!obj) {
          Logger.warn(`_confirmAccount: ${lookup} not found`)
          reject(404)
        } else if (obj.key === key) {
          Logger.info(`_confirmAccount: ${lookup} confirmed`)
          resolve({ account: account, key: key })
        } else {
          Logger.warn(`_confirmAccount: ${lookup} failed`)
          reject(403)
        }
      } catch (error) {
        Logger.warn('_confirmAccount failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param searchParams
   * @returns {Promise<any>}
   * @private
   */
  _getPeer (searchParams) {
    return new Promise((resolve, reject) => {
      if (!this._iroha) {
        return reject(503)
      }

      try {
        const address = Iroha.validatePeer(searchParams.get('address'))
        this._iroha.getPeers().then((peers) => {
          for (const key in Object.values(peers)) {
            if (peers[key].address === address) {
              return resolve({
                address: address,
                key: peers[key].peerKey
              })
            }
          }
          reject(404)
        })
      } catch (error) {
        Logger.warn('_getPeer failed').trace(error)
        reject(403)
      }
    })
  }

  /**
   * @param url
   * @returns {Promise<*>}
   * @private
   */
  _tunnelRequest (url) {
    return new Promise((resolve, reject) => {
      const options = {
        url: url,
        timeout: TIMEOUT_REQUEST_TUNNEL_MS,
        headers: {
          'User-Agent': 'diva-' + Date.now()
        }
      }
      if (/^http:\/\/[^/?#]+\.i2p(:[\d]+)?([/?#]|$)/.test(url)) {
        options.agent = tunnel.httpOverHttp({
          proxy: {
            host: this._hostnameI2P,
            port: this._portHttpProxyI2P,
            timeout: TIMEOUT_REQUEST_TUNNEL_MS,
            headers: {
              'User-Agent': 'diva-' + Date.now()
            }
          }
        })
      }

      get.concat(options, (error, response, data) => {
        if (!error && response.statusCode === 200) {
          resolve(data)
        } else {
          Logger.warn(`_tunnelRequest: failed to request ${url}`).trace(error || response.statusCode)
          reject(error || response.statusCode)
        }
      })
    })
  }

  /**
   * @param action {string}
   * @returns {string}
   * @private
   */
  static _validateAction (action) {
    switch (action) {
      case 'add':
      case 'remove':
        break
      default:
        throw new Error('invalid action')
    }
    return action
  }
}

module.exports = { IrohaNode }

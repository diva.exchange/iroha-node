/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import fs from 'fs-extra'
import grpc from 'grpc'
import {
  QueryService_v1Client as QueryService,
  CommandService_v1Client as CommandService
} from 'iroha-helpers/lib/proto/endpoint_grpc_pb'
import { commands, queries } from 'iroha-helpers'
import path from 'path'
import sodium from 'sodium-native'

const TIMEOUT_COMMAND = 5000
const PORT_DEFAULT_IROHA = 10001

export const REGEX_NAME_PEER = /^[a-z0-9-_.]{0,64}$/
export const REGEX_PUBLIC_KEY = /^[a-f0-9]{64}$/
export const REGEX_NAME_ACCOUNT = /^[a-z_0-9]{1,32}$/
export const REGEX_ID_DOMAIN = /^([a-zA-Z]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)*[a-zA-Z]([a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?$/

const ERROR_KEY_NOT_FOUND = 'iroha keys not found'
const ERROR_FAIL_CONNECT = 'failed to connect to iroha'
const ERROR_INVALID_NAME_PEER = 'invalid peer name'
const ERROR_INVALID_PUBLIC_KEY = 'invalid public key'
const ERROR_INVALID_NAME_ACCOUNT = 'invalid account name'
const ERROR_INVALID_ID_DOMAIN = 'invalid domain id'

export class Iroha {
  /**
   * Factory, create a new Iroha Instance
   *
   * @param torii {String} Example: localhost:50051
   * @param creatorAccountId {String} Example: myuser@mydomain - private key must be accessible
   * @returns {Promise<Iroha>}
   * @throws {Error}
   */
  static async make (torii, creatorAccountId) {
    return new Iroha(
      torii,
      creatorAccountId
    )
  }

  /**
   * @param torii {String} Examples: localhost:51151 or 127.0.0.1:50051
   * @param creatorAccountId {String} Example: myuser@mydomain - private key must be accessible
   * @returns {Promise<Iroha>}
   * @throws {Error}
   * @private
   */
  constructor (torii, creatorAccountId) {
    // load private key
    const pathPrivateKey = path.join(__dirname, `../../data/keys/${creatorAccountId}.priv`)
    if (!fs.existsSync(pathPrivateKey)) {
      throw new Error(ERROR_KEY_NOT_FOUND + ` (${pathPrivateKey})`)
    }
    try {
      return this._init(torii, creatorAccountId, pathPrivateKey)
    } catch (error) {
      throw new Error(ERROR_FAIL_CONNECT)
    }
  }

  /**
   * @param torii {String} Example: localhost:50051
   * @param creatorAccountId {String} Example: myuser@mydomain - private key must be accessible
   * @param pathPrivateKey {String}
   * @returns {Promise<Iroha>}
   * @private
   */
  async _init (torii, creatorAccountId, pathPrivateKey) {
    this._creatorAccountId = creatorAccountId

    /** @type {Buffer} */
    this._bufferCreatorPrivateKey = sodium.sodium_malloc(sodium.crypto_box_SECRETKEYBYTES)
    sodium.sodium_mlock(this._bufferCreatorPrivateKey)
    this._bufferCreatorPrivateKey.fill(Buffer.from(fs.readFileSync(pathPrivateKey).toString(), 'hex'))

    this._commandService = new CommandService(
      torii,
      grpc.credentials.createInsecure()
    )
    this._queryService = new QueryService(
      torii,
      grpc.credentials.createInsecure()
    )

    return this
  }

  /**
   * Add a new peer
   *
   * @param host {String} like testnet-abc123
   * @param peerKey {String}
   * @returns {Promise<Object>}
   * @throws {Error}
   * @public
   */
  async addPeer (host, peerKey) {
    const address = Iroha.validatePeer(host)
    await commands.addPeer(
      {
        privateKeys: [this._bufferCreatorPrivateKey],
        creatorAccountId: this._creatorAccountId,
        quorum: 1,
        commandService: this._commandService,
        timeoutLimit: TIMEOUT_COMMAND
      },
      {
        address: address,
        peerKey: Iroha.validateKey(peerKey)
      })

    return {
      address: address,
      peerKey: peerKey
    }
  }

  /**
   * Remove a peer
   *
   * @param publicKey {String}
   * @returns {Promise<Object>}
   * @throws {Error}
   * @public
   */
  async removePeer (publicKey) {
    await commands.removePeer(
      {
        privateKeys: [this._bufferCreatorPrivateKey],
        creatorAccountId: this._creatorAccountId,
        quorum: 1,
        commandService: this._commandService,
        timeoutLimit: TIMEOUT_COMMAND
      },
      {
        publicKey: Iroha.validateKey(publicKey)
      })

    return {
      publicKey: publicKey
    }
  }

  /**
   * @returns {Promise}
   * @throws {Error}
   * @public
   */
  async getPeers () {
    return queries.getPeers(
      {
        privateKey: this._bufferCreatorPrivateKey,
        creatorAccountId: this._creatorAccountId,
        queryService: this._queryService,
        timeoutLimit: TIMEOUT_COMMAND
      },
      {
      }
    )
  }

  /**
   * Create a new account
   *
   * @TODO look at quorum
   *
   * @param nameAccount {String}
   * @param idDomain {String}
   * @param publicKey {String}
   * @returns {Promise<Object>}
   * @throws {Error}
   * @public
   */
  async createAccount (nameAccount, idDomain, publicKey) {
    const response = await commands.createAccount(
      {
        privateKeys: [this._bufferCreatorPrivateKey],
        creatorAccountId: this._creatorAccountId,
        quorum: 1,
        commandService: this._commandService,
        timeoutLimit: TIMEOUT_COMMAND
      },
      {
        accountName: Iroha.validateNameAccount(nameAccount),
        domainId: Iroha.validateIdDomain(idDomain),
        publicKey: Iroha.validateKey(publicKey)
      })

    return {
      username: nameAccount,
      domain: idDomain,
      response: response
    }
  }

  /**
   * @param account {string}
   * @returns {string}
   * @throws {Error} If the account is invalid
   */
  static validateAccount (account) {
    const [nameAccount, idDomain] = account ? account.split('@') : ''
    Iroha.validateNameAccount(nameAccount)
    Iroha.validateIdDomain(idDomain)
    return account
  }

  /**
   * @param nameAccount {string}
   * @returns {string}
   * @throws {Error} If the username is invalid
   */
  static validateNameAccount (nameAccount) {
    if (REGEX_NAME_ACCOUNT.test(nameAccount)) {
      return nameAccount
    }
    throw new Error(ERROR_INVALID_NAME_ACCOUNT)
  }

  /**
   * @param idDomain {string}
   * @returns {string}
   * @throws {Error} If the domain is invalid
   */
  static validateIdDomain (idDomain) {
    if (REGEX_ID_DOMAIN.test(idDomain)) {
      return idDomain
    }
    throw new Error(ERROR_INVALID_ID_DOMAIN)
  }

  /**
   * @param peer {string}
   * @param defaultPort {number}
   * @returns {string}
   * @throws {Error} If the peer is invalid
   */
  static validatePeer (peer, defaultPort = PORT_DEFAULT_IROHA) {
    let [hostname, port] = peer.split(':')
    port = Number(port) > 0 && Number(port) <= 65535 ? Number(port) : defaultPort
    if (hostname && REGEX_NAME_PEER.test(hostname)) {
      return hostname + ':' + port
    }
    throw new Error(ERROR_INVALID_NAME_PEER)
  }

  /**
   * @param hostname {string}
   * @returns {string}
   * @throws {Error} If the peer is invalid
   */
  static validateHostname (hostname) {
    if (hostname && REGEX_NAME_PEER.test(hostname)) {
      return hostname
    }
    throw new Error(ERROR_INVALID_NAME_PEER)
  }

  /**
   * @param key {string}
   * @returns {string}
   * @throws {Error} If the key is invalid
   */
  static validateKey (key) {
    if (key && REGEX_PUBLIC_KEY.test(key)) {
      return key
    }
    throw new Error(ERROR_INVALID_PUBLIC_KEY)
  }
}

module.exports = { Iroha }

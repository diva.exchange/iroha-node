/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import { describe, it } from 'mocha'
import { Iroha } from '../src/iroha'

import * as chai from 'chai'
const assert = chai.assert

/**
 * Project: iroha-node
 * Context: iroha
 */
describe('#iroha-node#iroha#', () => {
  describe('Iroha Instantiation', function () {
    it('Factory', async () => {
      const i = await Iroha.make()
      assert.instanceOf(i, Iroha)
    })
  })

  describe('Iroha Peer Management', function () {
    this.timeout(30000)

    const namePeer = 'test1'
    const publicKey = '0000000000000000000000000000000000000000000000000000000000000001'

    it('addPeer', async () => {
      const iroha = await Iroha.make()
      const response = await iroha.addPeer(namePeer, publicKey)
      assert.isObject(response)
    })

    it('getPeers', async () => {
      const iroha = await Iroha.make()
      const response = await iroha.getPeers()
      assert.isArray(response)
      assert.equal(response.length, 4)
    })

    it('removePeer', async () => {
      const iroha = await Iroha.make()
      const response = await iroha.removePeer(publicKey)
      assert.isObject(response)
    })
  })
})

/**
 * Copyright (C) 2020 diva.exchange
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
 */

'use strict'

import fs from 'fs-extra'
import { IrohaNode } from './src/iroha-node'
import { Logger } from '@diva.exchange/diva-logger'
import path from 'path'

const config = _configure()
Logger.trace('Configuration').trace(config)

const _node = new IrohaNode(config)

process.once('SIGINT', () => {
  _node.shutdown().then(() => {
    process.exit(0)
  })
})

function _configure () {
  let _p
  const config = require('../package.json')[process.env.NODE_ENV === 'production' ? 'IrohaNode' : 'devIrohaNode']

  process.env.LOG_LEVEL = config.log_level = process.env.LOG_LEVEL || config.log_level ||
    (process.env.NODE_ENV === 'production' ? 'info' : 'trace')
  Logger.setOptions({ name: config.log_name || 'IrohaNode', level: config.log_level })

  // environment has precedence over config
  config.ip_listen = process.env.IP_LISTEN || config.ip_listen
  if (!config.ip_listen) {
    throw new Error('invalid ip')
  }

  config.port_listen = process.env.PORT_LISTEN || config.port_listen
  _p = Math.floor(Number(config.port_listen || 0))
  if (_p < 1025 || _p > 65535) {
    throw new Error('invalid port')
  }
  config.port_listen = _p

  config.torii = process.env.TORII || config.torii
  config.account_id = process.env.ACCOUNT_ID || config.account_id

  config.i2p_hostname = process.env.I2P_HOSTNAME || config.i2p_hostname
  if (!config.i2p_hostname) {
    throw new Error('invalid i2p hostname')
  }

  config.i2p_port_http_proxy = process.env.I2P_HTTP_PROXY_PORT || config.i2p_port_http_proxy
  _p = Math.floor(Number(config.i2p_port_http_proxy || 0))
  if (_p < 1025 || _p > 65535) {
    throw new Error('invalid i2p http proxy port')
  }
  config.i2p_port_http_proxy = _p

  config.i2p_port_webconsole = process.env.I2P_WEBCONSOLE_PORT || config.i2p_port_webconsole
  _p = Math.floor(Number(config.i2p_port_webconsole || 0))
  if (_p < 1025 || _p > 65535) {
    throw new Error('invalid webconsole port')
  }
  config.i2p_port_webconsole = _p

  config.bootstrap_peer = process.env.BOOTSTRAP_PEER || config.bootstrap_peer
  if (!Array.isArray(config.bootstrap_peer)) {
    config.bootstrap_peer = config.bootstrap_peer ? [config.bootstrap_peer.toString()] : []
  }

  config.path_iroha = process.env.PATH_IROHA || config.path_iroha || '/tmp/iroha/'

  config.API_BUILD = (new Date(fs.statSync(path.join(__dirname, '../node_modules')).mtime)).toISOString()

  return config
}

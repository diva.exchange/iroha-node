#
# Copyright (C) 2020 diva.exchange
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Author/Maintainer: Konrad Bächler <konrad@diva.exchange>
#

FROM node:lts-slim

LABEL author="Konrad Baechler <konrad@diva.exchange>" \
  maintainer="Konrad Baechler <konrad@diva.exchange>" \
  name="diva-iroha-node" \
  description="Distributed digital value exchange upholding security, reliability and privacy" \
  url="https://diva.exchange"

COPY app /home/node/app
COPY package.* /home/node/
COPY entrypoint.sh /

RUN apt-get update \
  && apt-get -y install \
    procps \
    xz-utils \
  && mkdir -p /home/node/data/keys \
  && cd /home/node/ \
  && npm install --only=production \
  && chown -R node:node /home/node/ \
  && chmod +x /entrypoint.sh

# API
EXPOSE 19012

VOLUME [ "/home/node/data/" ]
WORKDIR /home/node/
ENTRYPOINT [ "/entrypoint.sh" ]

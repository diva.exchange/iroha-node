# IMPORTANT

RETIRED!

Project has been migrated to diva.exchange/diva-api.

# Iroha Node

Iroha API application - glue between DIVA application and Iroha. Created by [DIVA.EXCHANGE](https://diva.exchange). 

## Get Started

DIVA.EXCHANGE offers preconfigured packages to start or join the DIVA.EXCHANGE Iroha testnet.

It's probably best to use the preconfigured package "diva-dockerized" (https://codeberg.org/diva.exchange/diva-dockerized).

For experienced users on an operating systems supporting Docker (Linux, Windows, MacOS) the following instructions will help to get started.

### Using Docker Compose

Clone the code repository from the public repository:
```
git clone -b master https://codeberg.org/diva.exchange/iroha-node.git
cd iroha-node
```

To start a local Iroha Node (proxy) make sure you have "Docker Compose" installed (https://docs.docker.com/compose/install/). Check your Docker Compose installation by executing `docker-compose --version` in a terminal.

If you have Docker Compose available, execute within your iroha-node folder:
```
sudo docker-compose up -d
```

To stop the container using Docker Compose, execute:
```
sudo docker-compose down
```
 
To stop the container, including the removal of the related volume (data of the container gets removed) using Docker Compose, execute:
```
sudo docker-compose down --volumes
```

## Contact the Developers

Talk to us via Telegram [https://t.me/diva_exchange_chat_de]() (English or German).

## Donations

Your donation goes entirely to the project. Your donation makes the development of DIVA.EXCHANGE faster.

XMR: 42QLvHvkc9bahHadQfEzuJJx4ZHnGhQzBXa8C9H3c472diEvVRzevwpN7VAUpCPePCiDhehH4BAWh8kYicoSxpusMmhfwgx

BTC: 3Ebuzhsbs6DrUQuwvMu722LhD8cNfhG1gs

Awesome, thank you!
